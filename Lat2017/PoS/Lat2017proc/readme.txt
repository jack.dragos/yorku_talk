%                                           
% LaTeX class for Web of Conferences Journal
% read-me file
%


This directory contains the LaTeX support for the Web of conferences Journal.

The EPJ WoC document class is derived from the LaTeX2e article.cls
based on TEX version 3.141 and LaTeX2e. You may use it with the LaTeX
engine or the PDFLaTeX engine. Be sure that the LaTeX version is at
least the 2007 version.
If you version is older than 2007, please update it.
If you are unable to easily update your old version, you may add some
specific styles in your local directory.



CONTENTS

  readme.txt       This file
  webofc.cls       Document class file for Web of conferences Journal
  webofc-doc.pdf   Main User's Guide Web of conferences Journal (PDF)
  woc.bst          Bibliography style for BibTeX users

  Lattice2017_indicoID_NAME.tex        Latex template file and ...
  Lattice2017_indicoID_NAME.pdf        ... corresponding pdf
  lattice2017.bib                      Example bibtex file
  epjconf_copyright_SIGN_AND_SCAN.pdf  License agreement to be signed and
                                       returned. 
  epjconf_instructions.pdf             Instructions for pdf file preparation 


INSTALLATION

Copy webofc.cls and woc.bst to a place where LaTeX can find them or simply copy
them in the same directory as the source file of the article.


GETTING STARTED

You should first read the "Main User's Guide" (webofc-doc.pdf) to get an
overview of special instructions concerning the Web of conferences journal.
Alternatively to reading the entire documentation, it is also possible to
proceed by merely filling out the template and then looking up additional
commands as needed.
